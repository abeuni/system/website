import * as firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/auth'

const config = {
  apiKey: 'AIzaSyCFQNsCH6oTiNHXnpgIKwjHxiRXwFdmt4M',
  authDomain: 'abeuni-system-230711.firebaseapp.com',
  databaseURL: 'https://abeuni-system-230711.firebaseio.com',
  projectId: 'abeuni-system-230711',
  storageBucket: 'abeuni-system-230711.appspot.com',
  messagingSenderId: '283602307757'
}

const auth = {
  google: new firebase.auth.GoogleAuthProvider(),
  facebook: new firebase.auth.FacebookAuthProvider()
}

const firebaseApp = firebase.apps[0] || firebase.initializeApp(config)
const db = firebaseApp.firestore()
const storage = firebaseApp.storage()

export { db, storage, auth, firebase }
